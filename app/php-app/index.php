<?php
session_start();
require "confDB.php";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Генератор случайных писателей">
  <meta name="keywords" content="случайный писатель, все писатели по алфавиту, генератор писателей, генератор случайного писателя, все деятели литературы, писатели википедия, писатели wiki">
  <title>Генератор случайных писателей</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" href="img/icon.ico" type="image/x-icon">
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(87551258, "init", {
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/87551258" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
  <section class="main-section w-100">
    <div class="container">
      <h1 class="title text-center pt-4">Генератор случайных писателей</h1>
      <div class="d-flex flex-column justify-content-center align-items-center alphabet-frame">

        <form action="script.php" method="POST">
          <div class="alphabet d-flex justify-content-center">
            <button name="letter" value='А'>А</button>
            <button name="letter" value='Б'>Б</button>
            <button name="letter" value='В'>В</button>
            <button name="letter" value='Г'>Г</button>
            <button name="letter" value='Д'>Д</button>
            <button name="letter" value='Е'>Е</button>
            <button name="letter" value='Ж'>Ж</button>
            <button name="letter" value='З'>З</button>
            <button name="letter" value='И'>И</button>
            <button name="letter" value='Й'>Й</button>
          </div>

          <div class="alphabet d-flex justify-content-center">
            <button name="letter" value='К'>К</button>
            <button name="letter" value='Л'>Л</button>
            <button name="letter" value='М'>М</button>
            <button name="letter" value='Н'>Н</button>
            <button name="letter" value='О'>О</button>
            <button name="letter" value='П'>П</button>
            <button name="letter" value='Р'>Р</button>
            <button name="letter" value='С'>С</button>
            <button name="letter" value='Т'>Т</button>
            <button name="letter" value='У'>У</button>
          </div>

          <div class="alphabet d-flex justify-content-center">
            <button name="letter" value='Ф'>Ф</button>
            <button name="letter" value='Х'>Х</button>
            <button name="letter" value='Ц'>Ц</button>
            <button name="letter" value='Ч'>Ч</button>
            <button name="letter" value='Ш'>Ш</button>
            <button name="letter" value='Щ'>Щ</button>
            <button name="letter" value='Э'>Э</button>
            <button name="letter" value='Ю'>Ю</button>
            <button name="letter" value='Я'>Я</button>
          </div>
          <div class="alphabet d-flex justify-content-center">
            <button id="all-btn" name="all" value='all'>Случайный автор</button>
          </div>  
        </form>

        <div class="line m-2"></div>

          <a href="
            <?php 
              if (isset($_SESSION['writter']['link'])){
                echo $_SESSION['writter']['link'];
              }
            ?>  
          " class="d-flex flex-row justify-content-center align-items-center writter" target="_blank">
            <?php 
              if (isset($_SESSION['writter']['name'])){
                echo $_SESSION['writter']['name'];
              }
              unset($_SESSION);
            ?>
            <div class="ml-4 arrow">&#8594;</div>
          </a>

        <div class="line m-2"></div>
      </div>
<!--LiveInternet counter--><a href="#">
    <img id="licnt1247" style="border:0" width="0" height="0"
src="img/li.png"
alt=""/></a><script>(function(d,s){d.getElementById("licnt1247").src=
"https://counter.yadro.ru/hit?t44.18;r"+escape(d.referrer)+
((typeof(s)=="undefined")?"":";s"+s.width+"*"+s.height+"*"+
(s.colorDepth?s.colorDepth:s.pixelDepth))+";u"+escape(d.URL)+
";h"+escape(d.title.substring(0,150))+";"+Math.random()})
(document,screen)</script><!--/LiveInternet-->
    </div>
  </section>
</body>
</html>
